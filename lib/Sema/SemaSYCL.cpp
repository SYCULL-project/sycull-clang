//===--- SemaSYCL.cpp - Semantic Analysis for SYCL constructs -------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
/// \file
/// \brief This file implements semantic analysis for SYCL constructs.
///
//===----------------------------------------------------------------------===//

#include "clang/ASTMatchers/ASTMatchFinder.h"
#include "clang/ASTMatchers/ASTMatchers.h"
#include "clang/Sema/Sema.h"
using namespace clang;

std::string getKernelName(const CXXMemberCallExpr *ParallelCallExpr) {

  const TemplateArgumentList *ArgumentList = cast<clang::TemplateArgumentList>(
      ParallelCallExpr->getMethodDecl()->getTemplateSpecializationArgs());

  std::string KernelName(ArgumentList->get(0).getAsType().getAsString());
  auto const pos = KernelName.find_last_of(' ');
  return KernelName.substr(pos + 1);
}

struct KernelHandler : public ast_matchers::MatchFinder::MatchCallback {
public:
  void run(const ast_matchers::MatchFinder::MatchResult &Result) override {
    const auto *ParallelS =
          Result.Nodes.getNodeAs<clang::CXXMemberCallExpr>("ParallelForMemberCallExpr");
    const auto *LambdaS =
              Result.Nodes.getNodeAs<clang::LambdaExpr>("ParallelForLambda");
    LambdaS->getLambdaClass()->addAttr(SYCLKernelAttr::CreateImplicit(LambdaS->getLambdaClass()->getASTContext(),getKernelName(ParallelS)));
    LambdaS->getLambdaClass()->getASTContext().addToSYCLKernelList(getKernelName(ParallelS),LambdaS->getLambdaClass());
  }
};

void Sema::createSYCLAttributes() {
  KernelHandler KernelFinder;

  ast_matchers::MatchFinder Finder;

  Finder.addMatcher(
      ast_matchers::lambdaExpr(
              ast_matchers::hasParent(
                  ast_matchers::materializeTemporaryExpr(
                      ast_matchers::hasParent(
                          ast_matchers::cxxConstructExpr(
                                      ast_matchers::hasParent(
                                          ast_matchers::cxxMemberCallExpr(
                                              ast_matchers::callee(
                                                  ast_matchers::
                                                      cxxMethodDecl(
                                                              ast_matchers::
                                                                  hasName(
                                                                      "parall"
                                                                      "el_"
                                                                      "fo"
                                                                      "r"))))
                                              .bind("ParallelForMemberCallExpr")))))))
          .bind("ParallelForLambda"),
      &KernelFinder);

  Finder.matchAST(this->getASTContext());
}
