//===----- CGSYCLNV.cpp - Interface to FRANGOLLO Runtime ------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This provides a class for SYCL code generation targeting the FRANGOLLO
// runtime library.
//
//===----------------------------------------------------------------------===//

#include "CGSYCLRuntime.h"
#include "CGCUDARuntime.h"
#include "CodeGenFunction.h"
#include "CodeGenModule.h"
#include "clang/AST/Decl.h"
#include "clang/AST/DeclCXX.h"
#include "clang/AST/ExprCXX.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/CallSite.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/IntrinsicInst.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include <map>
#include <tuple>
#include <list>
#include <vector>
#include <algorithm>
#include <string>

using namespace clang;
using namespace CodeGen;

namespace {

class CGNVSYCLRuntime : public CGSYCLRuntime {

private:
  llvm::Type *IntTy, *Int64Ty,  *SizeTy, *VoidTy, *FloatTy;
  llvm::PointerType *CharPtrTy, *VoidPtrTy, *VoidPtrPtrTy, *FloatPtrTy;

  /// Convenience reference to LLVM Context
  llvm::LLVMContext &Context;
  /// Convenience reference to the current module
  llvm::Module &TheModule;

  CGBuilderTy *CGBuilder;

  CodeGenFunction CGFunction;

  // A map with kernels
  std::map<llvm::StringRef,SYCLKernel> KernelsMap;

  // This maps establishes equivalence between string types and types
  std::map<std::string,llvm::Type*> TypesMap;

  void InitializeTypesMap();

public:
  CGNVSYCLRuntime(CodeGenModule &CGM);
  void EmitSingleKernel(llvm::StringRef *KernelStr, const CXXRecordDecl *CRD) override;
  void PopulateKernelStruct(llvm::StringRef *KernelName, const CXXMethodDecl *MD);
  void SetDimensions(const CXXMethodDecl *MD, SYCLKernel &Kernel);
  void CreateAnonParamList(const CXXMethodDecl *MD, std::list<std::tuple<std::string, llvm::Type*>> *Params);
  llvm::Function* CreateGeneratedFunction(const CXXMethodDecl *MD, llvm::StringRef *KernelName);
  std::vector<llvm::Type*> getParamsForFunction(llvm::StringRef KernelName);
  void EmitAllocaParams(SYCLKernel &Kernel);
  llvm::Function* CloneFunctionWithReplacements(llvm::Function *OldFunc, llvm::ValueToValueMapTy &VMap, llvm::StringRef *KernelName);
  llvm::BasicBlock* CloneBasicBlockWithReplacements(const llvm::BasicBlock *BB, llvm::ValueToValueMapTy &VMap, llvm::Function *F, bool FirstTime, SYCLKernel &Kernel);
};

}

CGNVSYCLRuntime::CGNVSYCLRuntime(CodeGenModule &CGM)
    : CGSYCLRuntime(CGM), Context(CGM.getLLVMContext()),
      TheModule(CGM.getModule()), CGFunction(CGM) {
  CodeGen::CodeGenTypes &Types = CGM.getTypes();
  ASTContext &Ctx = CGM.getContext();

  IntTy = Types.ConvertType(Ctx.IntTy);
  SizeTy = Types.ConvertType(Ctx.getSizeType());
  VoidTy = llvm::Type::getVoidTy(Context);
  FloatTy = llvm::Type::getFloatTy(Context);
  Int64Ty = llvm::Type::getInt64Ty(Context);

  CharPtrTy = llvm::PointerType::getUnqual(Types.ConvertType(Ctx.CharTy));
  VoidPtrTy = cast<llvm::PointerType>(Types.ConvertType(Ctx.VoidPtrTy));
  FloatPtrTy = llvm::Type::getFloatPtrTy(Context);

  VoidPtrPtrTy = VoidPtrTy->getPointerTo();

  CGBuilder = &CGFunction.Builder;

  InitializeTypesMap();
}

//Populate KernelMap with kernel info and emits the kernel
void CGNVSYCLRuntime::EmitSingleKernel(llvm::StringRef *KernelStr, const CXXRecordDecl *CRD) {

  for (auto it = CRD->methods().begin(); it != CRD->methods().end(); it++)
    if (it->getNameAsString() == "operator()") {
      PopulateKernelStruct(KernelStr,*it);

      SYCLKernel Kernel = KernelsMap.find(KernelStr->str())->second; //Get kernel struct

#if 0
      //Create kernel type
      auto Params = getParamsForFunction(KernelStr->str());
      llvm::FunctionType *FTy = llvm::FunctionType::get(VoidTy, Params, false);

      //Create kernel function
      TheModule.getOrInsertFunction(KernelStr->str(), FTy);
      Kernel.KernelFunction = TheModule.getFunction(KernelStr->str());

      //Set params names
      auto ParamIterator = Kernel.KernelAnonParams.begin();
      for (auto &Arg : Kernel.KernelFunction->args()) {
         Arg.setName(std::get<0>(*ParamIterator));
         ParamIterator++;
      }
#endif

      llvm::ValueToValueMapTy VMap;
      Kernel.KernelFunction = CloneFunctionWithReplacements(Kernel.GeneratedFunction, VMap, Kernel.KernelName);

      //Add kernel metadata
      llvm::NamedMDNode *MD = TheModule.getOrInsertNamedMetadata("nvvm.annotations");
      llvm::Metadata *MDVals[] = {
            llvm::ConstantAsMetadata::get(Kernel.KernelFunction), llvm::MDString::get(Context, "kernel"),
            llvm::ConstantAsMetadata::get(
                llvm::ConstantInt::get(llvm::Type::getInt32Ty(Context), 1))};
        // Append metadata to nvvm.annotations
      MD->addOperand(llvm::MDNode::get(Context, MDVals));

     Kernel.GeneratedFunction->removeFromParent(); //remove and leave only the one with the replacements

    }
}

void CGNVSYCLRuntime::PopulateKernelStruct(llvm::StringRef *KernelName, const CXXMethodDecl *MD){
  SYCLKernel NewKernel;

  NewKernel.KernelName = KernelName;
  NewKernel.OriginalMethodDecl = MD;
  NewKernel.KernelDimensions = 0;
  SetDimensions(MD, NewKernel);
  CreateAnonParamList(MD, &NewKernel.KernelAnonParams);
  NewKernel.GeneratedFunction = CreateGeneratedFunction(MD, KernelName);

  KernelsMap[KernelName->str()] = NewKernel;
}

void CGNVSYCLRuntime::SetDimensions(const CXXMethodDecl *MD, SYCLKernel &Kernel){
  for (auto it : MD->parameters()) {
    std::string TypeAsString = it->getType().getAsString();
    if (TypeAsString == "cl::sycl::id<1>") {
      Kernel.SYCLIndexDimX = it->getName();
      Kernel.KernelDimensions++;
    }
    if (TypeAsString == "cl::sycl::id<2>") {
      Kernel.SYCLIndexDimY = it->getName();
      Kernel.KernelDimensions++;
    }
    if (TypeAsString == "cl::sycl::id<3>") {
      Kernel.SYCLIndexDimZ = it->getName();
      Kernel.KernelDimensions++;
    }
  }
}

//Create param for each captured variabVMap[&BB] = CBB;le
void CGNVSYCLRuntime::CreateAnonParamList(const CXXMethodDecl *MD, std::list<std::tuple<std::string,llvm::Type*>> *Params) {
  for (auto it : MD->getParent()->captures()){
    auto MapIt = TypesMap.find(it.getCapturedVar()->getType().getAsString());
    if(MapIt != TypesMap.end()) {
      auto ParamTuple = std::make_tuple(it.getCapturedVar()->getNameAsString(), MapIt->second);
      Params->push_back(ParamTuple);
    }
  }
}

llvm::Function* CGNVSYCLRuntime::CreateGeneratedFunction(const CXXMethodDecl *MD, llvm::StringRef *KernelName) {
  const CGFunctionInfo &FI = CGM.getTypes().arrangeCXXMethodDeclaration(MD);
  llvm::FunctionType *Ty = CGM.getTypes().GetFunctionType(FI);

  TheModule.getOrInsertFunction(KernelName->str().append("_dummy"), Ty);
  llvm::Function *Fn = TheModule.getFunction(KernelName->str().append("_dummy"));
  CGFunction.GenerateCode(MD->getAsFunction(), Fn, FI);

  return Fn;
}

std::vector<llvm::Type*> CGNVSYCLRuntime::getParamsForFunction(llvm::StringRef KernelName) {
  std::vector<llvm::Type*> Params;
  SYCLKernel Kernel = KernelsMap.find(KernelName)->second;

  //only the types are taken
  for (auto it = Kernel.KernelAnonParams.begin(); it != Kernel.KernelAnonParams.end(); it++)
    Params.push_back(std::get<1>(*it));

  return Params;
}

void CGNVSYCLRuntime::InitializeTypesMap(){
  TypesMap["struct cl::sycl::accessor<float, 1, cl::sycl::access::mode::write, cl::sycl::access::target::global_buffer>"] = FloatPtrTy;
  TypesMap["struct cl::sycl::accessor<float, 1, cl::sycl::access::mode::read, cl::sycl::access::target::global_buffer>"] = FloatPtrTy;
  TypesMap["float *"] = FloatPtrTy;
  TypesMap["cl::sycl::id<1>"] = IntTy;
  TypesMap["cl::sycl::id<2>"] = IntTy;
  TypesMap["cl::sycl::id<3>"] = IntTy;
  TypesMap["int"] = IntTy;
}

void CGNVSYCLRuntime::EmitAllocaParams(SYCLKernel &Kernel) {

  auto DL = CGM.getDataLayout();
  int AnonParams = Kernel.KernelAnonParams.size();

  for (auto &Arg : Kernel.KernelFunction->args()) {
    if (AnonParams > 0) {
      Arg.dump();
      llvm::AllocaInst *AllocaArg = CGBuilder->CreateAlloca(Arg.getType(), nullptr, Arg.getName());
      AllocaArg->setAlignment(DL.getPrefTypeAlignment(Arg.getType()));
      Kernel.KernellNamedValuesMap[Arg.getName()] = AllocaArg;
      CGBuilder->CreateAlignedStore(&Arg, AllocaArg, DL.getPrefTypeAlignment(Arg.getType()), false);
      AnonParams--;
    }
  }
}

llvm::Function* CGNVSYCLRuntime::CloneFunctionWithReplacements(llvm::Function *OldFunc, llvm::ValueToValueMapTy &VMap, llvm::StringRef *KernelName) {
  std::vector<llvm::Type*> ArgTypes;
  SYCLKernel Kernel;
  int AnonParams;
  llvm::ValueMapTypeRemapper *TypeMapper = nullptr;
  llvm::ValueMaterializer *Materializer = nullptr;
  auto DL = CGM.getDataLayout();

  ArgTypes = getParamsForFunction(KernelName->str());
  AnonParams = ArgTypes.size();
  Kernel = KernelsMap.find(KernelName->str())->second;

  for (const llvm::Argument &I : OldFunc->args())
    if (I.getName() == "this") {
      if (const llvm::StoreInst *SI = dyn_cast<llvm::StoreInst>(*I.user_begin()))
        if (const llvm::AllocaInst *AI = dyn_cast<llvm::AllocaInst>(SI->getPointerOperand())) {
          Kernel.AnonStore = SI;
          Kernel.AnonAlloca = AI;
          Kernel.AnonLoad = dyn_cast<llvm::LoadInst>(*AI->user_begin());
        }
    }
    else
      ArgTypes.push_back(I.getType());

  llvm::FunctionType *FTy = llvm::FunctionType::get(OldFunc->getFunctionType()->getReturnType(), ArgTypes, OldFunc->getFunctionType()->isVarArg());
  llvm::Function *NewF = llvm::Function::Create(FTy, OldFunc->getLinkage(), KernelName->str(), OldFunc->getParent());

  llvm::Function::arg_iterator DestI = NewF->arg_begin();
  auto AnonParamIterator = Kernel.KernelAnonParams.begin();
  for (int i = 0; i < AnonParams; i++ ){
    DestI->setName(std::get<0>(*AnonParamIterator));
    AnonParamIterator++;
    DestI++;
  }

  for (const llvm::Argument &I : OldFunc->args())
    if (I.getName() != "this") {
      DestI->setName(I.getName());
      VMap[&I] = &*DestI++;
    }

  //Emit allocas and store for anon arguments and stablish map between GEPs
  for (auto &Arg : NewF->args()) {
    if (AnonParams > 0) {
      llvm::AllocaInst *AllocaArg = CGBuilder->CreateAlloca(Arg.getType(), nullptr, Arg.getName());
      AllocaArg->setAlignment(DL.getPrefTypeAlignment(Arg.getType()));
      Kernel.AllocaArgList.push_back(AllocaArg);
      AllocaArg->removeFromParent();

      Kernel.KernellNamedValuesMap[Arg.getName()] = AllocaArg;
      llvm::StoreInst *StoreArg = CGBuilder->CreateAlignedStore(&Arg, AllocaArg, DL.getPrefTypeAlignment(Arg.getType()), false);
      Kernel.StoreArgList.push_back(StoreArg);
      StoreArg->removeFromParent();

      llvm::LoadInst *LoadArg = CGBuilder->CreateAlignedLoad(AllocaArg, AllocaArg->getAlignment());
      for (auto it : Kernel.AnonLoad->users())
        if (const llvm::GetElementPtrInst *GEPI = dyn_cast<llvm::GetElementPtrInst>(it))
          if (const llvm::ConstantInt *C = dyn_cast<llvm::ConstantInt>(GEPI->getOperand(2)))
            if (C->getSExtValue() == Arg.getArgNo()) {
              const llvm::LoadInst *LoadGEPI = dyn_cast<llvm::LoadInst>(*GEPI->user_begin());
              Kernel.AnonGEPMapList.push_back(std::make_tuple(LoadArg, GEPI, LoadGEPI));
            }
      LoadArg->removeFromParent();

      AnonParams--;
    }
  }

  llvm::AttributeSet NewAttrs = NewF->getAttributes();
  NewF->copyAttributesFrom(OldFunc);
  NewF->setAttributes(NewAttrs);

  llvm::AttributeSet OldAttrs = OldFunc->getAttributes();
  NewF->setAttributes(
      NewF->getAttributes()
          .addAttributes(NewF->getContext(), llvm::AttributeSet::ReturnIndex,
                         OldAttrs.getRetAttributes())
          .addAttributes(NewF->getContext(), llvm::AttributeSet::FunctionIndex,
                         OldAttrs.getFnAttributes()));

  llvm::SmallVector<std::pair<unsigned, llvm::MDNode *>, 1> MDs;
  OldFunc->getAllMetadata(MDs);
  for (auto MD : MDs)
    NewF->addMetadata(
        MD.first,
        *MapMetadata(MD.second, VMap, llvm::RF_None, TypeMapper, Materializer));

  bool FirstBB = true;
  for (llvm::Function::const_iterator BI = OldFunc->begin(), BE = OldFunc->end();
       BI != BE; ++BI) {
    const llvm::BasicBlock &BB = *BI;

    llvm::BasicBlock *CBB = CloneBasicBlockWithReplacements(&BB, VMap, NewF, FirstBB, Kernel);

    VMap[&BB] = CBB;

    if (BB.hasAddressTaken()) {
      llvm::Constant *OldBBAddr = llvm::BlockAddress::get(const_cast<llvm::Function*>(OldFunc),
                                              const_cast<llvm::BasicBlock*>(&BB));
      VMap[OldBBAddr] = llvm::BlockAddress::get(NewF, CBB);
    }

    FirstBB = false;

  }

  for (llvm::Function::iterator BB = cast<llvm::BasicBlock>(VMap[&OldFunc->front()])->getIterator(), BE = NewF->end(); BB != BE; ++BB)
    // Loop over all instructions, fixing each one as we find it...
    for (llvm::Instruction &II : *BB)
      llvm::RemapInstruction(&II, VMap, llvm::RF_None, TypeMapper, Materializer);

  return NewF;
}

llvm::BasicBlock* CGNVSYCLRuntime::CloneBasicBlockWithReplacements(const llvm::BasicBlock *BB, llvm::ValueToValueMapTy &VMap, llvm::Function *F, bool FirstTime, SYCLKernel &Kernel) {
  llvm::BasicBlock *NewBB = llvm::BasicBlock::Create(BB->getContext(), "", F);

  //if it is the first BB, create a Br from Allocated params block
  if (FirstTime) {
    for (auto it : Kernel.AllocaArgList)
      NewBB->getInstList().push_back(it);
    for (auto it : Kernel.StoreArgList)
      NewBB->getInstList().push_back(it);
  }

  if (BB->hasName())
    NewBB->setName(BB->getName());

  bool hasCalls = false, hasDynamicAllocas = false, hasStaticAllocas = false;

  // Loop over all instructions, and copy them over.
  for (llvm::BasicBlock::const_iterator II = BB->begin(), IE = BB->end(); II != IE; ++II) {
    //skip references to class.anon
    if (Kernel.AnonAlloca == dyn_cast<llvm::AllocaInst>(II))
      continue;
    if (Kernel.AnonStore == dyn_cast<llvm::StoreInst>(II))
      continue;
    if (Kernel.AnonLoad == dyn_cast<llvm::LoadInst>(II))
      continue;

    //If it a GEP to the anon class, skip it
    bool skipGEP = false;
    for (auto it : Kernel.AnonGEPMapList)
      if (std::get<1>(it) == dyn_cast<llvm::GetElementPtrInst>(II))
        skipGEP = true;
    if (skipGEP)
      continue;

    bool ReplaceLoadInst = false;
    for (auto it : Kernel.AnonGEPMapList) {
      if (std::get<2>(it) == dyn_cast<llvm::LoadInst>(II)) {
        llvm::Instruction *NewInst = II->clone();
        if (II->hasName())
          NewInst->setName(II->getName());
        NewBB->getInstList().push_back(std::get<0>(it));
        VMap[&*II] = std::get<0>(it); //map to the NEW load
        ReplaceLoadInst = true;
        continue;
      }
    }
    if (ReplaceLoadInst)
      continue; //skip the rest, already introduced

    llvm::Instruction *NewInst = II->clone();
    if (II->hasName())
      NewInst->setName(II->getName());
    NewBB->getInstList().push_back(NewInst);
    VMap[&*II] = NewInst; // Add instruction map to value.

    hasCalls |= (isa<llvm::CallInst>(II) && !isa<llvm::DbgInfoIntrinsic>(II));
    if (const llvm::AllocaInst *AI = dyn_cast<llvm::AllocaInst>(II)) {
      if (isa<llvm::ConstantInt>(AI->getArraySize()))
        hasStaticAllocas = true;
      else
        hasDynamicAllocas = true;
    }
  }

  return NewBB;
}

CGSYCLRuntime *CodeGen::CreateNVSYCLRuntime(CodeGenModule &CGM) {
  return new CGNVSYCLRuntime(CGM);
}
