//===----- CGSYCL.h - Interface to SYCL -------------------------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This provides a class for SCYL code generation for a kernel targeting 
// the FRANGOLLO runtime library.
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_CLANG_LIB_CODEGEN_CGSYCLRUNTIME_H
#define LLVM_CLANG_LIB_CODEGEN_CGSYCLRUNTIME_H

#include <list>
#include <tuple>
#include <string>
#include <map>

namespace llvm {
  class StringRef;
  class Type;
  class AllocaInst;
  class StoreInst;
  class LoadInst;
  class GetElementPtrInst;
  class Function;
  class BasicBlock;
}

namespace clang {

class CXXRecordDecl;
class CXXMethodDecl;

namespace CodeGen {

class CodeGenModule;

struct SYCLKernel {
  llvm::StringRef *KernelName = nullptr;
  const clang::CXXMethodDecl *OriginalMethodDecl = nullptr;
  std::list<std::tuple<std::string,llvm::Type*>> KernelAnonParams;
  const llvm::StoreInst* AnonStore;
  const llvm::AllocaInst* AnonAlloca;
  const llvm::LoadInst* AnonLoad;
  std::list<std::tuple<llvm::LoadInst*,const llvm::GetElementPtrInst*, const llvm::LoadInst*>> AnonGEPMapList;
  std::list<llvm::AllocaInst*> AllocaArgList;
  std::list<llvm::StoreInst*> StoreArgList;
  llvm::Function* GeneratedFunction;
  llvm::Function* KernelFunction;
  //number of dimensions of the kernel
  int KernelDimensions;
  //Name of the indexes used by SYCL
  std::string SYCLIndexDimX;
  std::string SYCLIndexDimY;
  std::string SYCLIndexDimZ;
  //Map between variable names and values used by the kernel
  std::map<std::string, llvm::AllocaInst *> KernellNamedValuesMap;
  //Kernel block address
  llvm::BasicBlock *KernelBlock;
  //Return block address
  llvm::BasicBlock *ReturnBlock;
};

class CGSYCLRuntime {

protected:
  CodeGenModule &CGM;

public:

  CGSYCLRuntime(CodeGenModule &CGM) : CGM(CGM) {}
  virtual ~CGSYCLRuntime();

  virtual void EmitSingleKernel(llvm::StringRef *KernelStr, const clang::CXXRecordDecl *CRD);

};

/// Creates an instance of a SYCL runtime class.
CGSYCLRuntime *CreateNVSYCLRuntime(CodeGenModule &CGM);

}
}

#endif
