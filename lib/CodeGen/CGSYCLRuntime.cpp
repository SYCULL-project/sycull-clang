//===----- CGSYCLRuntime.cpp - Interface to SYCL Runtimes -----------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This provides an abstract class for SYCL code generation.  Concrete
// subclasses of this implement code generation for specific SYCL
// runtime libraries.
//
//===----------------------------------------------------------------------===//

#include "CGSYCLRuntime.h"
#include "clang/AST/DeclCXX.h"

using namespace clang;
using namespace CodeGen;

CGSYCLRuntime::~CGSYCLRuntime() {}

void CGSYCLRuntime::EmitSingleKernel(llvm::StringRef *KernelStr, const CXXRecordDecl *CRD) {}
